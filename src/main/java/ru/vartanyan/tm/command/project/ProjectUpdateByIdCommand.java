package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-update-by-id";
    }

    @Override
    public String description() {
        return "Update project by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER Id]");
        @NotNull final String id = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(id, userId);
        System.out.println("[ENTER NAME]");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[INTER DESCRIPTION]");
        @NotNull final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().updateEntityById(id, name, description, userId);
        System.out.println("[PROJECT UPDATED]");
    }

}
